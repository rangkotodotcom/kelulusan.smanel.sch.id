<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?= $title ?></title>

    <!-- Custom fonts for this template-->
    <link href="<?= base_url('assets/') ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template-->
    <link href="<?= base_url('assets/') ?>css/sb-admin.css" rel="stylesheet">
    <link rel="icon" href="<?= base_url('assets/') ?>img/icon.png" type="image/png">

</head>

<body class="bg-dark">

    <div class="container">
        <div class="alert" data-flashdata="<?= $this->session->flashdata('alert'); ?>"></div>
        <div class="card card-register mx-auto mt-5 mb-5">
            <div class="card-header">Form Surat Berkelakuan Baik</div>
            <div class="card-body">
                <?php

                $hasil = $this->session->flashdata('hasil');

                // var_dump($lahir);

                if (!$this->session->flashdata('hasil')) {

                ?>

                    <form action="<?= base_url('skbb/cari/') ?>" method="post" id="user">
                        <div class="form-group">
                            <div class="form-label-group">
                                <input type="text" id="inputNisn" class="form-control" placeholder="NISN" name="nisn" value="<?= set_value('nisn') ?>">
                                <?= form_error('nisn', '<small class="text-danger pl-3">', '</small>'); ?>
                                <label for="inputNisn">NISN</label>
                            </div>
                        </div>
                        <button class="btn btn-primary btn-lg">Cari</button>
                    </form>

                <?php } else {
                    $lahir = $hasil['t_lahir'] . ', ' . date_indo($hasil['tgl_lhr']);
                ?>


                    <h5 class="card-text">Silahkan periksa terlebih dahulu data diri anda. Jika terdapat kesalahan, silahkan perbaiki ke ICT <?= $hasil["sekolah"]; ?>.</h5><br>

                    <form action="<?= base_url('skbb/cetak/') . $hasil["nisn"] ?>" method="post" id="user">
                        <div class="form-group">
                            <div class="form-label-group">
                                <input type="text" id="inputNama" class="form-control-plaintext" readonly value="<?= $hasil["nama"]; ?>" name="nama">
                                <label for="inputNama">Nama Lengkap</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-label-group">
                                <input type="text" id="inputNama" class="form-control-plaintext" readonly value="<?= $lahir; ?>">
                                <label for="inputNama">Tempat, Tanggal Lahir</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-label-group">
                                <input type="text" id="inputNisn" class="form-control-plaintext" readonly name="nisn" value="<?= $hasil["nisn"]; ?>" name="nisn">
                                <label for="inputNisn">NISN</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-label-group">
                                <input type="text" id="inputNis" class="form-control-plaintext" readonly value="<?= $hasil["nis"]; ?>">
                                <label for="inputNis">NIS</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-label-group">
                                <input type="text" id="inputKelas" class="form-control-plaintext" readonly value="<?= $hasil["kelas"]; ?>">
                                <label for="inputKelas">Kelas</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-label-group">
                                <input type="text" id="inputJurusan" class="form-control-plaintext" readonly value="<?= $hasil["jur"]; ?>">
                                <label for="inputJurusan">Jurusan</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">Pernyataan siswa</label>
                            <textarea class="form-control-plaintext" id="exampleFormControlTextarea1" rows="4" readonly>Saya menyatakan data tersebut benar dan saya berkelakuan baik selama menempuh pendidikan di <?= $hasil["sekolah"]; ?>. Apabila pernyataan saya tidak benar, maka saya bertanggungjawab penuh untuk menerima konsekuensinya.</textarea>
                        </div>
                        <div class="form-group">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" id="gridCheck" name="perjanjian" required>
                                <label class="form-check-label" for="gridCheck">
                                    Saya menyetujui pernyataan diatas
                                </label>
                            </div>
                        </div>

                        <button class="btn btn-primary btn-block">Cetak</button>

                    </form>

                <?php } ?>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="<?= base_url('assets/') ?>vendor/jquery/jquery.min.js"></script>
    <script src="<?= base_url('assets/') ?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="<?= base_url('assets/') ?>vendor/jquery-easing/jquery.easing.min.js"></script>

    <script src="<?= base_url('assets/') ?>js/sweetalert2.all.min.js"></script>
    <script src="<?= base_url('assets/') ?>js/alert1.js"></script>

</body>

</html>