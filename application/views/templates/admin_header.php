<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?= $title; ?></title>

    <!-- Custom fonts for this template-->
    <link href="<?= base_url('assets/') ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="<?= base_url('assets/') ?>css/sb-admin.min.css" rel="stylesheet">
    <link href="<?= base_url('assets/') ?>css/timeline/timeline.css" rel="stylesheet">
    <link href="<?= base_url('assets/') ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="<?= base_url('assets/') ?>vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <link href="<?= base_url('assets/') ?>vendor/timeline/dist/css/timeline.min.css" rel="stylesheet" />
    <link href="<?= base_url('assets/') ?>css/select2.min.css" rel="stylesheet" />
    <link rel="icon" href="<?= base_url('assets/') ?>img/icon.png" type="image/png">

</head>

<body id="page-top">
    <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

        <a class="navbar-brand mr-1" href="<?= base_url('admin/') ?>">
            <?php if ($this->session->userdata('level') == 'admin') { ?>
                ADMIN
            <?php } else if ($this->session->userdata('level') == 'pp') { ?>
                PUSTAKA
            <?php } else { ?>
                TATAUSAHA
            <?php } ?>
        </a>

        <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
            <i class="fas fa-bars"></i>
        </button>
        <!-- Navbar -->

        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown no-arrow">
                <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="mr-2 d-none d-lg-inline text-gray-600 small"><b><?= $user['nama']; ?></b></span>
                    <img class="img-profile rounded-circle" src="<?= base_url('upload/admin/' . $user['foto']) ?>" style="width:2rem; height:2rem;">
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                    <a class="dropdown-item" href="<?= base_url('setting/profil/') ?>">Profil</a>
                    <a class="dropdown-item" href="<?= base_url('setting/logaktifitas/') ?>">Log Aktifitas</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">Logout</a>
                </div>
            </li>
        </ul>

    </nav>
    <!-- Sidebar -->

    <div id="wrapper">
        <ul class="sidebar navbar-nav">
            <!-- Nav Item - Dashboard -->
            <li class="nav-item <?php if ($this->uri->segment(1) == "admin") {
                                    echo "active";
                                } ?>">
                <a class="nav-link" href="<?= base_url('admin/') ?>">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span></a>
            </li>

            <?php if ($this->session->userdata('level') == 'admin') { ?>

                <!-- Nav Item - Pages Collapse Menu -->
                <li class="nav-item dropdown <?php if ($this->uri->segment(2) == "profilsekolah" or $this->uri->segment(2) == "blangkosurat" or $this->uri->segment(2) == "isisurat") {
                                                    echo "active";
                                                } ?>">
                    <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-fw fa-home"></i>
                        <span>Data Sekolah</span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="pagesDropdown">
                        <h6 class="dropdown-header">Data Sekolah:</h6>
                        <a class="dropdown-item <?php if ($this->uri->segment(2) == "profilsekolah") {
                                                    echo "active";
                                                } ?>" href="<?= base_url('master/profilsekolah/') ?>">Profil Sekolah</a>
                        <a class="dropdown-item <?php if ($this->uri->segment(2) == "blangkosurat") {
                                                    echo "active";
                                                } ?>" href="<?= base_url('master/blangkosurat/') ?>">Blangko Surat</a>
                        <!-- <a class="dropdown-item <?php if ($this->uri->segment(2) == "isisurat") {
                                                            echo "active";
                                                        } ?>" href="<?= base_url('master/isisurat/') ?>">Isi Surat</a> -->
                    </div>
                </li>

                <!-- Nav Item - Pages Collapse Menu -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-fw fa-users"></i>
                        <span>Data Siswa</span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="pagesDropdown">
                        <h6 class="dropdown-header">Data Siswa:</h6>
                        <a class="dropdown-item" href="<?= base_url('master/datadiri/') ?>">Data Diri</a>
                        <a class="dropdown-item" href="<?= base_url('master/nilaiun/') ?>">Nilai UN</a>
                        <a class="dropdown-item" href="<?= base_url('master/nilaiusbn/') ?>">Nilai USBN</a>
                        <a class="dropdown-item" href="<?= base_url('master/nilairapor/') ?>">Nilai Rapor</a>
                        <a class="dropdown-item" href="<?= base_url('master/admsiswa/') ?>">Adm Siswa</a>
                    </div>
                </li>

                <!-- Nav Item - History -->
                <li class="nav-item <?php if ($this->uri->segment(2) == "informasi") {
                                        echo "active";
                                    } ?>">
                    <a class="nav-link" href="<?= base_url('master/informasi/') ?>">
                        <i class="fas fa-fw fa-bullhorn"></i>
                        <span>Informasi</span></a>
                </li>

                <!-- Nav Item - History -->
                <li class="nav-item <?php if ($this->uri->segment(2) == "izindownload") {
                                        echo "active";
                                    } ?>">
                    <a class="nav-link" href="<?= base_url('master/izindownload/') ?>">
                        <i class="fas fa-fw fa-user-lock"></i>
                        <span>Akses Donwload</span></a>
                </li>

                <!-- Nav Item - History -->
                <li class="nav-item <?php if ($this->uri->segment(2) == "skbbprint") {
                                        echo "active";
                                    } ?>">
                    <a class="nav-link" href="<?= base_url('master/skbbprint/') ?>">
                        <i class="fas fa-fw fa-print"></i>
                        <span>SKB Print</span></a>
                </li>

                <!-- Nav Item - History -->
                <li class="nav-item <?php if ($this->uri->segment(2) == "naikkelas") {
                                        echo "active";
                                    } ?>">
                    <a class="nav-link" href="<?= base_url('master/naikkelas/') ?>">
                        <i class="fas fa-fw fa-graduation-cap"></i>
                        <span>Naik Kelas</span></a>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="importDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-fw fa-file-import"></i>
                        <span>Import</span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="importDropdown">
                        <h6 class="dropdown-header">Import Data:</h6>
                        <a class="dropdown-item" href="<?= base_url('master/importsiswa/') ?>">Import Siswa</a>
                        <a class="dropdown-item" href="<?= base_url('master/importnilaiun/') ?>">Import Nilai UN</a>
                        <a class="dropdown-item" href="<?= base_url('master/importnilaius/') ?>">Import Nilai USBN</a>
                        <a class="dropdown-item" href="<?= base_url('master/importnilairapor/') ?>">Import Nilai Rapor</a>
                        <a class="dropdown-item" href="<?= base_url('master/importnaikkelas/') ?>">Import Kenaikan Kelas</a>
                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="systemDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-fw fa-cogs"></i>
                        <span>Sistem</span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="systemDropdown">
                        <h6 class="dropdown-header">Pengaturan Sistem:</h6>
                        <a class="dropdown-item" href="<?= base_url('master/dataadmin/') ?>">Data Admin</a>
                        <a class="dropdown-item" href="<?= base_url('master/waktupengumuman/') ?>">Waktu Pengumuman</a>
                        <a class="dropdown-item" href="<?= base_url('master/tahundata/') ?>">Tahun Data</a>
                    </div>
                </li>

                <!-- Nav Item - History -->
                <li class="nav-item <?php if ($this->uri->segment(2) == "cp") {
                                        echo "active";
                                    } ?>">
                    <a class="nav-link" href="<?= base_url('master/cp/') ?>">
                        <i class="fas fa-fw fa-phone"></i>
                        <span>Contact Person</span></a>
                </li>

                <!-- Divider -->
                <hr class="sidebar-divider d-none d-md-block">


            <?php } else if ($this->session->userdata('level') == 'pp') { ?>

                <!-- Nav Item - History -->
                <li class="nav-item <?php if ($this->uri->segment(2) == "pustaka") {
                                        echo "active";
                                    } ?>">
                    <a class="nav-link" href="<?= base_url('page/pustaka/') ?>">
                        <i class="fas fa-fw fa-user-alt"></i>
                        <span>Data Siswa</span></a>
                </li>

            <?php } else { ?>

                <!-- Nav Item - History -->
                <li class="nav-item <?php if ($this->uri->segment(2) == "tatausaha") {
                                        echo "active";
                                    } ?>">
                    <a class="nav-link" href="<?= base_url('page/tatausaha/') ?>">
                        <i class="fas fa-fw fa-user-alt"></i>
                        <span>Data Siswa</span></a>
                </li>

            <?php } ?>

        </ul>

        <div id="content-wrapper">
            <div class="container-fluid">