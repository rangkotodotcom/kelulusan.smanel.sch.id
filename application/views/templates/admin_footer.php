</div>
<!-- /.container-fluid -->

<div class="alert" data-flashdata="<?= $this->session->flashdata('alert'); ?>"></div>

<!-- Sticky Footer -->
<footer class="sticky-footer">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright &copy; 2019 <a style="text-decoration: none; color:black;" target="_blank" href="https://instagram.com/jamilur.kotomambang">Kali</a></span>
        </div>
    </div>
</footer>

</div>
<!-- /.content-wrapper -->

</div>
<!-- /#wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Yakin Ingin Keluar?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Pilih "Logout" jika kamu yakin dan ingin keluar dari web ini!.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href="<?= base_url('auth/'); ?>logout">Logout</a>
            </div>
        </div>
    </div>
</div>


<!-- Bootstrap core JavaScript-->
<script src=" <?= base_url('assets/') ?>vendor/jquery/jquery.min.js"> </script>
<script src="<?= base_url('assets/') ?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="<?= base_url('assets/') ?>vendor/jquery-easing/jquery.easing.min.js"></script>
<!-- timeline -->
<script src="<?= base_url('assets/') ?>vendor/timeline/dist/js/timeline.min.js"></script>
<script>
    $('.timeline').timeline({
        forceVerticalMode: 800,
        mode: 'vertical',
        visibleItems: 4
    });
</script>

<!-- CkEditor -->
<script src="<?= base_url('assets/') ?>js/ckeditor/ckeditor.js"></script>
<script src="<?= base_url('assets/') ?>js/ckeditor/build-config.js"></script>
<script src="<?= base_url('assets/') ?>js/ckeditor/config.js"></script>
<script src="<?= base_url('assets/') ?>js/ckeditor/contents.js"></script>
<script src="<?= base_url('assets/') ?>js/ckeditor/styles.js"></script>
<script>
    CKEDITOR.replace('isi');
</script>

<!-- Custom scripts for all pages-->
<script src="<?= base_url('assets/') ?>js/sb-admin.min.js"></script>
<script src="<?= base_url('assets/') ?>js/select2.min.js"></script>
<script src="<?= base_url('assets/') ?>js/sweetalert2.all.min.js"></script>
<script src="<?= base_url('assets/') ?>js/alert2.js"></script>

<!-- Page level plugins -->
<script src="<?= base_url('assets/') ?>vendor/chart.js/Chart.min.js"></script>

<!-- Page level plugin JavaScript-->
<script src="<?= base_url('assets/') ?>vendor/chart.js/Chart.min.js"></script>
<script src="<?= base_url('assets/') ?>vendor/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url('assets/') ?>vendor/datatables/dataTables.bootstrap4.js"></script>

<!-- Page level custom scripts -->
<script src="<?= base_url('assets/') ?>js/demo/datatables-demo.js"></script>
<script src="<?= base_url('assets/') ?>js/demo/chart-area-demo.js"></script>
<script src="<?= base_url('assets/') ?>js/demo/chart-pie-demo.js"></script>

<script>
    $('.costum-file-input').on('change', function() {
        let fileName = $(this).val().split('\\').pop();
        $(this).next('.costum-file-label').addClass("selected").html(fileName);
    });

    $(document).ready(function() {
        $('.js-example-basic-single').select2();
    });
</script>

</body>

</html>