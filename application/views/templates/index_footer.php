<!-- Footer -->
<footer class="text-black">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright &copy; 2019 <a style="text-decoration: none; color:black;" target="_blank" href="https://instagram.com/jamilur.kotomambang">Kali</a></span>
        </div>
    </div>
</footer>
<!-- End of Footer -->


<!-- Bootstrap core JavaScript-->
<script src="<?= base_url('assets/') ?>vendor/jquery/jquery.min.js"></script>
<script src="<?= base_url('assets/') ?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- countdown -->
<script src="<?= base_url('assets/') ?>vendor/countdown/dscountdown.min.js"></script>

<?php if ($this->uri->segment(1) == "" or $this->uri->segment(1) == "hasil") { ?>
    <script>
        jQuery(document).ready(function($) {

            $('.buka').dsCountDown({
                endDate: new Date("<?= $system->waktu_pengumuman ?>"),
                theme: 'flat'
            });

        });
    </script>

<?php } ?>

<!-- Core plugin JavaScript-->
<script src="<?= base_url('assets/') ?>vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="<?= base_url('assets/') ?>js/sb-admin.min.js"></script>
<script src="<?= base_url('assets/') ?>js/sweetalert2.all.min.js"></script>
<script src="<?= base_url('assets/') ?>js/alert1.js"></script>

</body>

</html>