<!-- Breadcrumbs-->
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        Dashboard
    </li>
</ol>

<!-- Content Row -->
<div class="row">

    <div class="col-xl-3 col-sm-6 mb-3">
        <div class="card text-white bg-primary o-hidden h-100">
            <div class="card-body">
                <div class="card-body-icon">
                    <i class="fas fa-fw fa-calendar"></i>
                </div>
                <div class="mr-5">
                    <div class="text-xs font-weight-bold text-gray text-uppercase mb-1">Tahun Aktif</div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $system['tahun_data'] ?></div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-3 col-sm-6 mb-3">
        <div class="card text-white bg-warning o-hidden h-100">
            <div class="card-body">
                <div class="card-body-icon">
                    <i class="fas fa-fw fa-user"></i>
                </div>
                <div class="mr-5">
                    <div class="text-xs font-weight-bold text-gray text-uppercase mb-1">Data Siswa</div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800">
                        <?php if ($this->session->userdata('level') == 'admin') {
                            echo $jumlahsiswa;
                        } else if ($this->session->userdata('level') == 'pp') {
                            echo $jumlahsiswapustaka;
                        } else {
                            echo $jumlahsiswatatausaha;
                        } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Content Row -->

<div class="row">

    <!-- Area Chart -->
    <div class="col-xl-8 col-lg-7">
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Pengumuman</h6>
            </div>
            <!-- Card Body -->
            <div class="card-body">

                <?php
                if ($this->session->userdata('level') == 'admin') {
                    $informasi = $informasiadmin;
                } else if ($this->session->userdata('level') == 'pp') {
                    $informasi = $informasipustaka;
                } else {
                    $informasi = $informasitatausaha;
                }
                ?>

                <div class="timeline">
                    <div class="timeline__wrap">
                        <div class="timeline__items">

                            <?php foreach ($informasi as $im) { ?>

                                <div class="timeline__item">
                                    <div class="timeline__content">
                                        <h2>
                                            <?= $im->subjek ?>
                                        </h2>
                                        <div class="small">
                                            <?= tanggal_indo($im->tanggal_kirim) ?>
                                        </div>
                                        <p><?= $im->isi ?></p>
                                    </div>
                                </div>

                            <?php } ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Pie Chart -->
    <div class="col-xl-4 col-lg-5">
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Tim Support</h6>
            </div>
            <!-- Card Body -->
            <div class="card-body">
                <div class="chart-pie pt-4 pb-2">
                    <div class="row">
                        <?php foreach ($contactperson as $cp) : ?>
                            <div class="col-xl-6 font-weight-bold"><?= $cp->nama; ?></div>
                            <div class="col-xl-6 font-weight-bolder"><?= $cp->no_hp; ?></div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>