<?php

if ($profilsekolah['daerah'] == "Kab") {
    $dati_kop = "KABUPATEN";
    $dati_pem = "Kabupaten";
    $dati = "Kabupaten/<s>Kota</s>";
} else {
    $dati_kop = "KOTA";
    $dati_pem = "Kota";
    $dati = "<s>Kabupaten/</s>Kota";
}

?>

<!DOCTYPE html>
<html>

<head>
    <title>Surat Keterangan</title>
</head>

<body>
    <div class="header">
        <center>
            <table style="text-align:center; width:100%; border-bottom:3px solid;">
                <tr>
                    <td align="center">
                        <img src="<?= base_url('upload/logo/' . $profilsekolah['logo_prov']) ?>" width="70" />
                    </td>
                    <td style="font-size:16px;">
                        PEMERINTAH PROVINSI <?= strtoupper($profilsekolah['prov']);  ?><br>
                        <span style="font-size:14px;">DINAS PENDIDIKAN</span><br>
                        <span style="font-size:18x;"><?= $profilsekolah['nama_sekolah'];  ?></span><br>
                        <?= $dati_kop ?> <?= strtoupper($profilsekolah['kab_kota']);  ?><br>
                    </td>
                    <td align="center">
                        <img src="<?= base_url('upload/logo/' . $profilsekolah['logo_sekolah']) ?>" width="80" />
                    </td>
                </tr>
                <tr align="center">
                    <td colspan="3" style="font-size:10px;">Alamat : <?= $profilsekolah['alamat'];  ?> Telp : <?= $profilsekolah['telp'];  ?> </td>
                </tr>
            </table>

        </center>

    </div>

    <div class="content">
        <h4 align="center"><u><b>SURAT KETERANGAN</b></u><br><span style="font-size:10px;">NOMOR: 420/ /SMA.01/EL/2020</span></h4>

        <p style="font-size:12px;">Yang Bertanda Tangan di bawah ini Kepala <?= $profilsekolah['nama_sekolah']; ?> Dengan ini Menerangkan:</p><br>

        <table style="font-size:12px; margin-left:50px;" cellpadding="1">
            <tr>
                <td>Nama</td>
                <td> : </td>
                <td><b><?= $data['nama']; ?></b></td>
            </tr>
            <tr>
                <td>NIS / NISN</td>
                <td> : </td>
                <td><?= $data['nis']; ?> / <?= $data['nisn']; ?></td>
            </tr>
            <tr>
                <td>Kelas</td>
                <td> : </td>
                <td><?= $data['kelas']; ?></td>
            </tr>

        </table><br>

        <p align="justify">Berdasarkan Keputusan Rapat Kenaikan Kelas pada Tanggal 17 Juni 2020 dengan ini menerangkan bahwa yang bersangkutan dinyatakan </p>

        <h3 align="center" style="font-family:all;">
            <b>
                <?php if ($data['ket'] == 'N') { ?>
                    Naik / <s>Tidak Naik</s>
                <?php } else { ?>
                    <s>Naik</s> / Tidak Naik
                <?php } ?>
            </b>
        </h3>
        <p align="justify">Demikian surat keterangan ini dibuat, atas perhatian diucapkan terima kasih.</p>
    </div>

    <div class="footer" style="margin-top: 1em;">
        <table style="text-align:center; width:100%; ">
            <tr>
                <td align="right" style="padding-right:40px;">
                    <img src="<?= base_url('upload/logo/ttd_sk.jpg'); ?>" width="255" />
                </td>
            </tr>
        </table>
    </div>

</body>

</html>