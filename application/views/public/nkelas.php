<div class="container">
    <div class="alert" data-flashdata="<?= $this->session->flashdata('alert'); ?>"></div>
    <!-- Outer Row -->
    <div class="row justify-content-center" style="margin-top: 6%">
        <div class="col-lg-7">
            <div class="card o-hidden border-0 shadow-lg my-4">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h2 text-gray-900 mb-4 font-weight-bold">Pengumuman Kenaikan Kelas</h1>
                                    <hr>
                                </div>

                                <?php if ($system->akses == 'buka') { ?>

                                    <form class="user" method="post" action="<?= base_url('nkelas/hasil/'); ?>">
                                        <fieldset>
                                            <legend class="h5">Masukkan NISN dan Tanggal Lahir</legend>
                                            <div class=form-group>
                                                <label>NISN</label>
                                                <input type="text" name="nisn" id="nisn" class="form-control" value="<?php if ($this->session->flashdata('hasil')) {
                                                                                                                            echo $this->session->flashdata('hasil')['nisn'];
                                                                                                                        } else {
                                                                                                                            echo set_value('nisn');
                                                                                                                        }
                                                                                                                        ?>">
                                                <?= form_error('nisn', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                            <div class=form-group>
                                                <label>Tanggal Lahir</label>
                                                <input name="tgl_lhr" type="date" class="form-control" value="<?php if ($this->session->flashdata('hasil')) {
                                                                                                                    echo $this->session->flashdata('hasil')['tgl_lhr'];
                                                                                                                } else {
                                                                                                                    echo set_value('tgl_lhr');
                                                                                                                }
                                                                                                                ?>">
                                                <?= form_error('tgl_lhr', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                        </fieldset>
                                        <button name="hasil" type="submit" class="btn btn-primary">Lihat Hasil</button>
                                    </form>

                                <?php } else { ?>

                                    <fieldset align="center">
                                        <legend class="h5">Link dapat diakses dalam</legend>
                                        <div class="buka"></div>
                                    </fieldset>

                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>