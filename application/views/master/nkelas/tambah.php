<!-- Breadcrumbs-->
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="<?= base_url('admin/'); ?>">Dashboard</a>
    </li>
    <li class="breadcrumb-item">
        <a href="<?= base_url('master/datadiri'); ?>">Data Kenaikan Kelas</a>
    </li>
    <li class="breadcrumb-item active">Tambah</li>
</ol>

<div class="card mb-3">
    <div class="card-header">
        <i class="fas fa-table"></i>
        Form Tambah Data Kenaikan Kelas</div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" width="100%" cellspacing="0">
                <form action="<?= base_url('master/naikkelas/add'); ?>" method="post">
                    <div class="form-group">
                        <input type="text" class="form-control form-control-lg" name="nama" id="nama" placeholder="Nama Lengkap" value="<?= set_value('nama'); ?>">
                        <?= form_error('nama', '<small class="text-danger pl-3">', '</small>'); ?>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control form-control-lg" name="t_lahir" id="t_lahir" placeholder="Tempat Lahir" value="<?= set_value('t_lahir'); ?>">
                        <?= form_error('t_lahir', '<small class="text-danger pl-3">', '</small>'); ?>
                    </div>
                    <div class="form-group">
                        <input type="date" class="form-control form-control-lg" name="tgl_lhr" id="tgl_lhr" placeholder="Tanggal Lahir" value="<?= set_value('tgl_lhr'); ?>">
                        <?= form_error('tgl_lhr', '<small class="text-danger pl-3">', '</small>'); ?>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control form-control-lg" name="nis" id="nis" placeholder="NIS" value="<?= set_value('nis'); ?>">
                        <?= form_error('nis', '<small class="text-danger pl-3">', '</small>'); ?>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control form-control-lg" name="nisn" id="nisn" placeholder="NISN" value="<?= set_value('nisn'); ?>">
                        <?= form_error('nisn', '<small class="text-danger pl-3">', '</small>'); ?>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control form-control-lg" name="kelas" id="kelas" placeholder="Kelas" value="<?= set_value('kelas'); ?>">
                        <?= form_error('kelas', '<small class="text-danger pl-3">', '</small>'); ?>
                    </div>
                    <div class="form-group">
                        <select class="form-control form-control-lg" name="ket" id="ket" value="<?= set_value('ket'); ?>">
                            <option value="">Keterangan</option>
                            <option value="N">Naik Kelas</option>
                            <option value="TN">Tidak Naik Kelas</option>
                        </select>
                        <?= form_error('ket', '<small class="text-danger pl-3">', '</small>'); ?>
                    </div>

                    <button type="submit" name="tambah" class="btn btn-primary">
                        Simpan
                    </button>
                </form>
            </table>
        </div>
    </div>
    </a>
</div>