<!-- Breadcrumbs-->
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="<?= base_url('admin/'); ?>">Dashboard</a>
    </li>
    <li class="breadcrumb-item active">Data Kenaikan Kelas</li>
</ol>

<div class="card mb-3">
    <div class="card-header">
        <i class="fas fa-user"></i>
        Data Kenaikan Kelas</div>
    <div class="card-body">
        <a class="btn btn-success btn-sm mb-3" href="<?= base_url('master/naikkelas/add'); ?>">Tambah</a>
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>NISN</th>
                        <th>Nama Lengkap</th>
                        <th>Tempat Lahir</th>
                        <th>Tanggal Lahir</th>
                        <th>NIS</th>
                        <th>Kelas</th>
                        <th>Ket</th>
                        <th width="15%">Aksi</th>
                    </tr>
                </thead>

                <tbody>
                    <?php $i = 1; ?>
                    <?php foreach ($naikkelas as $nk) : ?>

                        <tr>
                            <td><?= $i++ ?></td>
                            <td><?= $nk->nisn ?></td>
                            <td><?= $nk->nama ?></td>
                            <td><?= $nk->t_lahir ?></td>
                            <td><?= date_indo($nk->tgl_lhr) ?></td>
                            <td><?= $nk->nis ?></td>
                            <td><?= $nk->kelas ?></td>
                            <td align="center">
                                <?php if ($nk->ket == 'N') { ?>
                                    <span class="badge badge-info"> Naik Kelas</span>
                                <?php } else { ?>
                                    <span class="badge badge-danger"> Tidak Naik Kelas</span>
                                <?php } ?>
                            </td>
                            <td>
                                <a href="<?= base_url('master/naikkelas/edit/') . $nk->id; ?>" class="btn btn-success btn-sm">
                                    <i class="fas fa-edit"></i>
                                </a>
                                <a href="<?= base_url('master/naikkelas/delete/') . $nk->id; ?>" class="btn btn-danger btn-sm tombol-hapus">
                                    <i class="fas fa-eraser"></i>
                                </a>
                            </td>
                        </tr>

                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>