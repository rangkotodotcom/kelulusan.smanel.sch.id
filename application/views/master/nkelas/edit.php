<!-- Breadcrumbs-->
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="<?= base_url('admin/'); ?>">Dashboard</a>
    </li>
    <li class="breadcrumb-item">
        <a href="<?= base_url('master/naikkelas'); ?>">Data Kenaikan Kelas</a>
    </li>
    <li class="breadcrumb-item active">Edit</li>
</ol>

<div class="card mb-3">
    <div class="card-header">
        <i class="fas fa-table"></i>
        Form Edit Data Kenaikan Kelas</div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" width="100%" cellspacing="0">
                <form action="<?= base_url('master/naikkelas/edit/') . $naikkelas->id ?>" method="post">
                    <input type="hidden" name="id" value="<?= $naikkelas->id ?>">
                    <div class="form-group">
                        <input type="text" class="form-control form-control-lg" name="nama" id="nama" value="<?= $naikkelas->nama ?>">
                        <?= form_error('nama', '<small class="text-danger pl-3">', '</small>'); ?>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control form-control-lg" name="t_lahir" id="t_lahir" value="<?= $naikkelas->t_lahir ?>">
                        <?= form_error('t_lahir', '<small class="text-danger pl-3">', '</small>'); ?>
                    </div>
                    <div class="form-group">
                        <input type="date" class="form-control form-control-lg" name="tgl_lhr" id="tgl_lhr" value="<?= $naikkelas->tgl_lhr ?>">
                        <?= form_error('tgl_lhr', '<small class="text-danger pl-3">', '</small>'); ?>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control form-control-lg" name="nis" id="nis" value="<?= $naikkelas->nis ?>">
                        <?= form_error('nis', '<small class="text-danger pl-3">', '</small>'); ?>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control form-control-lg" name="nisn" id="nisn" value="<?= $naikkelas->nisn ?>" readonly>
                        <?= form_error('nisn', '<small class="text-danger pl-3">', '</small>'); ?>
                    </div>
                    <div class="form-group">
                        <select class="form-control form-control-lg" name="ket" id="ket">
                            <option value="">Keterangan</option>
                            <option value="N" <?php if ($naikkelas->ket == 'N') {
                                                    echo "selected";
                                                }  ?>>Naik Kelas</option>
                            <option value="TN" <?php if ($naikkelas->ket == 'TN') {
                                                    echo "selected";
                                                } ?>>Tidak Naik Kelas</option>
                        </select>
                        <?= form_error('ket', '<small class="text-danger pl-3">', '</small>'); ?>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control form-control-lg" name="kelas" id="kelas" value="<?= $naikkelas->kelas ?>">
                    </div>
                    <div class="form-group">
                        <input type="hidden" id="tahun" name="tahun" value="<?= $naikkelas->tahun ?>">
                    </div>

                    <button type="submit" name="edit" class="btn btn-primary">
                        Edit
                    </button>
                </form>
            </table>
        </div>
    </div>
    </a>
</div>