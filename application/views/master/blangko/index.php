<!-- Breadcrumbs-->
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="<?= base_url('admin/'); ?>">Dashboard</a>
    </li>
    <li class="breadcrumb-item active">Blangko Surat</li>
</ol>

<div class="row">

    <!-- Area Chart -->
    <div class="col-xl-9 col-lg-8">
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div class="card-header">
                <i class="fas fa-envelope"></i>
                Blangko SKL</div>
            <!-- Card Body -->
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped" width="100%" cellspacing="0">
                        <tbody>
                            <tr>
                                <td>Nama Surat</td>
                                <td><?= $blangkoskl->nama_surat ?></td>
                            </tr>
                            <tr>
                                <td>Nomor Surat</td>
                                <td><?= $blangkoskl->nomor_surat ?></td>
                            </tr>
                            <tr>
                                <td>Tempat dan Tanggal Surat</td>
                                <td><?= $blangkoskl->tempat_surat ?>, <?= tanggal(date("j F Y", strtotime($blangkoskl->tanggal_surat))) ?></td>
                            </tr>
                            <tr>
                                <td>Update Terakhir</td>
                                <td><?= tanggal_indo($blangkoskl->last_update) ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer">
                <a class="btn btn-primary btn-md" href="<?= base_url('master/blangkosurat/edit/') . $blangkoskl->id; ?>">Edit</a>
            </div>
        </div>
    </div>
    <!-- Pie Chart -->
    <div class="col-xl-3 col-lg-4">
        <div class="row">
            <div>
                <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div class="card-header">
                        <i class="fas fa-image"></i>
                        Tanda Tangan</div>
                    <!-- Card Body -->
                    <div class="card-body">
                        <div class="pt-4 pb-2">
                            <img src="<?= base_url('upload/logo/' . $blangkoskl->ttd) ?>" width="200" class="img-thumbnail" alt="Tanda Tangan" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">

    <!-- Area Chart -->
    <div class="col-xl-9 col-lg-8">
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div class="card-header">
                <i class="fas fa-envelope"></i>
                Blangko Capaian Kompetensi</div>
            <!-- Card Body -->
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped" width="100%" cellspacing="0">
                        <tbody>
                            <tr>
                                <td>Nama Surat</td>
                                <td><?= $blangkokompetensi->nama_surat ?></td>
                            </tr>
                            <tr>
                                <td>Nomor Surat</td>
                                <td><?= $blangkokompetensi->nomor_surat ?></td>
                            </tr>
                            <tr>
                                <td>Tempat dan Tanggal Surat</td>
                                <td><?= $blangkokompetensi->tempat_surat ?>, <?= tanggal(date("j F Y", strtotime($blangkokompetensi->tanggal_surat))) ?></td>
                            </tr>
                            <tr>
                                <td>Update Terakhir</td>
                                <td><?= tanggal_indo($blangkokompetensi->last_update) ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer">
                <a class="btn btn-primary btn-md" href="<?= base_url('master/blangkosurat/edit/') . $blangkokompetensi->id; ?>">Edit</a>
            </div>
        </div>
    </div>
    <!-- Pie Chart -->
    <div class="col-xl-3 col-lg-4">
        <div class="row">
            <div>
                <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div class="card-header">
                        <i class="fas fa-image"></i>
                        Tanda Tangan</div>
                    <!-- Card Body -->
                    <div class="card-body">
                        <div class="pt-4 pb-2">
                            <img src="<?= base_url('upload/logo/' . $blangkokompetensi->ttd) ?>" width="200" class="img-thumbnail" alt="Tanda Tangan" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">

    <!-- Area Chart -->
    <div class="col-xl-9 col-lg-8">
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div class="card-header">
                <i class="fas fa-envelope"></i>
                Blangko SKBB</div>
            <!-- Card Body -->
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped" width="100%" cellspacing="0">
                        <tbody>
                            <tr>
                                <td>Nama Surat</td>
                                <td><?= $blangkoskbb->nama_surat ?></td>
                            </tr>
                            <tr>
                                <td>Nomor Surat</td>
                                <td><?= $blangkoskbb->nomor_surat ?></td>
                            </tr>
                            <tr>
                                <td>Tempat dan Tanggal Surat</td>
                                <td><?= $blangkoskbb->tempat_surat ?>, <?= tanggal(date("j F Y", strtotime($blangkoskbb->tanggal_surat))) ?></td>
                            </tr>
                            <tr>
                                <td>Update Terakhir</td>
                                <td><?= tanggal_indo($blangkoskbb->last_update) ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer">
                <a class="btn btn-primary btn-md" href="<?= base_url('master/blangkosurat/edit/') . $blangkoskbb->id; ?>">Edit</a>
            </div>
        </div>
    </div>
    <!-- Pie Chart -->
    <div class="col-xl-3 col-lg-4">
        <div class="row">
            <div>
                <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div class="card-header">
                        <i class="fas fa-image"></i>
                        Tanda Tangan</div>
                    <!-- Card Body -->
                    <div class="card-body">
                        <div class="pt-4 pb-2">
                            <img src="<?= base_url('upload/logo/' . $blangkoskbb->ttd) ?>" width="200" class="img-thumbnail" alt="Tanda Tangan" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>