<!-- Breadcrumbs-->
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="<?= base_url('admin/'); ?>">Dashboard</a>
    </li>
    <li class="breadcrumb-item">
        <a href="<?= base_url('master/blangkosurat'); ?>">Blangko Surat</a>
    </li>
    <li class="breadcrumb-item active">Edit</li>
</ol>

<div class="card mb-3">
    <div class="card-header">
        <i class="fas fa-table"></i>
        Form Edit Blangko Surat</div>
    <div class="card-body">
        <div class="row">
            <div class="col-lg">
                <form action="<?= base_url('master/blangkosurat/edit/') . $blangkosurat->id ?>" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="id" value="<?= $blangkosurat->id ?>">
                    <div class="form-group row">
                        <label for="nama_surat" class="col-sm-2 col-form-label">Nama Surat</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="nama_surat" value="<?= $blangkosurat->nama_surat ?>" id="nama_surat">
                            <?= form_error('nama_surat', '<small class="text-danger pl-3">', '</small>'); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nomor_surat" class="col-sm-2 col-form-label">Nomor Surat</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="nomor_surat" value="<?= $blangkosurat->nomor_surat ?>" id="nomor_surat">
                            <?= form_error('nomor_surat', '<small class="text-danger pl-3">', '</small>'); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="tempat_surat" class="col-sm-2 col-form-label">Tempat Surat</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="tempat_surat" value="<?= $blangkosurat->tempat_surat ?>" id="tempat_surat">
                            <?= form_error('tempat_surat', '<small class="text-danger pl-3">', '</small>'); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="tanggal_surat" class="col-sm-2 col-form-label">Tanggal Surat</label>
                        <div class="col-sm-10">
                            <input type="date" class="form-control" name="tanggal_surat" value="<?= $blangkosurat->tanggal_surat ?>" id="tanggal_surat">
                            <?= form_error('tanggal_surat', '<small class="text-danger pl-3">', '</small>'); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-2">TTD (ukuran max 314 x 220)</div>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-sm-3">
                                    <img src="<?= base_url('upload/logo/' . $blangkosurat->ttd) ?>" class="img-thumbnail" alt="Image">
                                </div>
                                <div class="col-sm-9">
                                    <div class="custom-file">
                                        <input type="file" class="form-control-file" id="ttd" name="ttd">
                                        <input type="hidden" id="old_ttd" name="old_ttd" value="<?= $blangkosurat->ttd ?>">
                                        <label class="custom-file-label" for="ttd">Choose file</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="submit" name="edit" class="btn btn-primary">
                        Edit
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>