<?php

if ($skl['ket'] == 'L') {
    $ket = "LULUS";
} else {
    $ket = "TIDAK LULUS";
}

?>

<!DOCTYPE html>
<html>

<head>
    <title>Surat Keterangan Lulus</title>
</head>

<body>
    <div class="header">
        <center>
            <table style="text-align:center; width:100%; border-bottom:3px solid;">
                <tr>
                    <td align="center">
                        <img src="<?= base_url('assets/img/prov.png') ?>" width="70" />
                    </td>
                    <td style="font-size:16px;">
                        PEMERINTAH PROVINSI SUMATERA BARAT<br>
                        <span style="font-size:14px;">DINAS PENDIDIKAN</span><br>
                        <span style="font-size:18x;">SMAN 1 ENAM LINGKUNG</span><br>
                        KABUPATEN PADANG PARIAMAN<br>
                    </td>
                    <td align="center">
                        <img src="<?= base_url('assets/img/pdd.png') ?>" width="80" />
                    </td>
                </tr>
                <tr align="center">
                    <td colspan="3" style="font-size:10px;">Alamat : jl. Raya Padang-Bukittinggi KM. 40, Parit Malintang Telp : 0751-96318 </td>
                </tr>
            </table>


        </center>

    </div>

    <div class="content">
        <h2 align="center"><u><b>SURAT KETERANGAN</b></u><br><span style="font-size:12px;">NOMOR:421/228/SMAN.01/EL/2019</span></h2>
        <p style="font-size:13px;">Yang bertanda tangan dibawah ini, Kepala SMA Negeri 1 ENAM LINGKUNG , Kabupaten Padang Pariaman, Provinsi Sumatera Barat dengan ini menerangkan bahwa </p><br>
        <table style="font-size:14px;" cellpadding="1">
            <tr>
                <td>Nama</td>
                <td> : </td>
                <td><?= $skl['nama']; ?></td>
            </tr>
            <tr>
                <td>NIS / NISN</td>
                <td> : </td>
                <td><?= $skl['nis']; ?> / <?= $skl['nisn']; ?></td>
            </tr>
            <tr>
                <td>Nomor Peserta</td>
                <td> : </td>
                <td><?= $skl['no_pes']; ?></td>
            </tr>
            <tr>
                <td>Kelas / Jurusan</td>
                <td> : </td>
                <td><?= $skl['kelas']; ?></td>
            </tr>

        </table><br>

        <p align="justify">Berdasarkan Hasil Ujian Nasional Berbasis Komputer (UNBK) yang dilaksanakan pada Tanggal 1,2,4 dan 8 April 2019 dan Pelaksanaan Ujian Sekolah Berbasis Nasional (USBN-BK) serta (USBN-KP) yang dilaksanakan pada tanggal 13 s/d 27 Maret 2019. Keputusan Rapat Dinas Pimpinan Sekolah dan Majelis Guru tentang Kelulusan Siswa Kelas XII Tahun Pelajaran 2018/2019 Pada tanggal 13 Mei 2019 dengan ini menyatakan yang namanya tersebut di atas : </p>

        <h2 align="center" style="font-family:all;"><b><?= $ket; ?></b></h2>
        <p align="justify">Demikian surat keterangan ini kami berikan untuk dapat dipergunakan oleh yang bersangkutan seperlunya, terima kasih.</p>
    </div>

    <div class="footer">
        <table style="text-align:center; width:100%; ">
            <tr>
                <td align="right" style="padding-left:220px;">
                    <img src="<?= base_url('upload/siswa/' . $skl["foto"]); ?>" width="100" />
                </td>
                <td align="left" style="padding-left:2px;">
                    <img src="<?= base_url('assets/img/ttd.png'); ?>" width="250" />
                </td>
            </tr>
        </table>
    </div>

</body>

</html>