<?php

if ($profilsekolah['daerah'] == "Kab") {
    $dati_kop = "KABUPATEN";
    $dati_pem = "Kabupaten";
} else {
    $dati_kop = "KOTA";
    $dati_pem = "Kota";
}

?>

<!DOCTYPE html>
<html>

<head>
    <title>Surat Keterangan Berkelakuan Baik</title>
</head>

<body>
    <div class="header">
        <center>
            <table style="text-align:center; width:100%; border-bottom:3px solid;">
                <tr>
                    <td align="center">
                        <img src="<?= base_url('upload/logo/' . $profilsekolah['logo_prov']) ?>" width="70" />
                    </td>
                    <td style="font-size:16px;">
                        PEMERINTAH PROVINSI <?= strtoupper($profilsekolah['prov']);  ?><br>
                        <span style="font-size:14px;">DINAS PENDIDIKAN</span><br>
                        <span style="font-size:18x;"><?= $profilsekolah['nama_sekolah'];  ?></span><br>
                        <?= $dati_kop ?> <?= strtoupper($profilsekolah['kab_kota']);  ?><br>
                    </td>
                    <td align="center">
                        <img src="<?= base_url('upload/logo/' . $profilsekolah['logo_sekolah']) ?>" width="80" />
                    </td>
                </tr>
                <tr align="center">
                    <td colspan="3" style="font-size:10px;">Alamat : <?= $profilsekolah['alamat'];  ?> Telp : <?= $profilsekolah['telp'];  ?> </td>
                </tr>
            </table>


        </center>

    </div>

    <div class="content">
        <h2 align="center"><u><b><?= $blangkoskbb['nama_surat'] ?></b></u><br><span style="font-size:12px;">NOMOR: <?= $blangkoskbb['nomor_surat'] ?></span></h2>
        <p style="font-size:13px;">Saya yang bertanda tangan dibawah ini, Kepala SMA Negeri 1 ENAM LINGKUNG , <?= $dati_pem ?> <?= $profilsekolah['kab_kota']; ?>, Provinsi Sumatera Barat dengan ini menerangkan bahwa </p><br>
        <table style="font-size:14px;" cellpadding="1" style="padding-left: 70px">
            <tr>
                <td style="width: 60%">NAMA</td>
                <td> : </td>
                <td><?= strtoupper($skbb['nama']); ?></td>
            </tr>
            <tr>
                <td style="width: 60%">Tanggal Lahir</td>
                <td> : </td>
                <td><?= date_indo($skbb['tgl_lhr']); ?></td>
            </tr>
            <tr>
                <td style="width: 60%">NISN</td>
                <td> : </td>
                <td><?= $skbb['nisn']; ?></td>
            </tr>
            <tr>
                <td style="width: 60%">NIS</td>
                <td> : </td>
                <td><?= $skbb['nis']; ?></td>
            </tr>

        </table><br>

        <p align="justify">yang bersangkutan selama menduduki pendidikan di SMA Negeri 1 Enam Lingkung , <?= $dati_pem ?> <?= $profilsekolah['kab_kota']; ?>, Provinsi Sumatera Barat. Pada tahun Pelajaran <?= $profilsekolah['tahun_ajaran']; ?>. Dengan "<b>Berkelakuan Baik</b>"</p>

        <p align="justify">Demikian surat keterangan ini kami berikan untuk dapat dipergunakan oleh yang bersangkutan seperlunya, terima kasih.</p>
    </div>

    <div class="footer">
        <table style="text-align:center; width:100%;">
            <tr>
                <td align="right" style="padding-right:20px;">
                    <img src="<?= base_url('upload/logo/' . $blangkoskbb["ttd"]); ?>" width="250" />
                </td>
            </tr>
        </table>
    </div>

</body>

</html>