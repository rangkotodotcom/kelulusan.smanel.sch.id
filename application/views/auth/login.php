<div class="container">
    <div class="alert" data-flashdata="<?= $this->session->flashdata('alert'); ?>"></div>
    <div class="card card-login mx-auto mt-5">
        <div class="card-header">Login Admin</div>
        <div class="card-body">
            <form class="user" method="post" action="<?= base_url('auth'); ?>">
                <div class="form-group">
                    <div class="form-label-group">
                        <input type="email" id="inputEmail" class="form-control" name="email" id="email" placeholder="Masukan Email Anda" value="<?= set_value('email'); ?>">
                        <?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>
                        <label for="inputEmail">Email</label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-label-group">
                        <input type="password" id="inputPassword" class="form-control" name="password" id="password" placeholder="Password">
                        <?= form_error('password', '<small class="text-danger pl-3">', '</small>'); ?>
                        <label for="inputPassword">Password</label>
                    </div>
                </div>
                <button type="submit" name="login" class="btn btn-primary btn-block">
                    Login
                </button>
            </form>
            <div class="text-center">
                <a class="d-block small mt-3" href="<?= base_url('auth/lupapassword'); ?>">Lupa Password?</a>
                <a class="d-block small" href="<?= base_url(''); ?>">Halaman Utama</a>
            </div>
        </div>
    </div>
</div>