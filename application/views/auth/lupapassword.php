<div class="container">
    <div class="alert" data-flashdata="<?= $this->session->flashdata('alert'); ?>"></div>
    <div class="card card-login mx-auto mt-5">
        <div class="card-header">Reset Password</div>
        <div class="card-body">
            <div class="text-center mb-4">
                <h4>Anda Lupa Password?</h4>
                <p>Kami akan mengirim password baru anda. Silahkan masukan email anda yang terdaftar disistem kami.</p>
            </div>
            <form class="user" method="post" action="<?= base_url('auth/lupapassword'); ?>">
                <div class="form-group">
                    <div class="form-label-group">
                        <input type="email" id="inputEmail" class="form-control" name="email" placeholder="Masukan Email Anda">
                        <label for="inputEmail">Alamat Email</label>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary btn-block">Reset Password</button>
            </form>
            <div class="text-center mt-2">
                <a class="d-block small" href="<?= base_url('auth'); ?>">Halaman Login</a>
            </div>
        </div>
    </div>
</div>