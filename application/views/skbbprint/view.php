<!-- Breadcrumbs-->
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="<?= base_url('admin/'); ?>">Dashboard</a>
    </li>
    <li class="breadcrumb-item active">Cetak SKBB</li>
</ol>

<div class="card mb-3">
    <div class="card-header">
        <i class="fas fa-file-download"></i>
        Data Cetak SKBB</div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Lengkap</th>
                        <th>NISN</th>
                        <th>Status Cetak</th>
                        <th>Waktu Cetak</th>
                    </tr>
                </thead>

                <tbody>
                    <?php $i = 1; ?>
                    <?php foreach ($skbbprint as $sp) : ?>

                        <tr>
                            <td><?= $i ?></td>
                            <td><?= $sp->nama ?></td>
                            <td><?= $sp->nisn ?></td>
                            <td align="center">
                                <?php if ($sp->status_cetak == '1') { ?>
                                    <span class="badge badge-success">
                                        <i class="fas fa-check-double fa-2x"></i>
                                    </span>
                                <?php } else { ?>
                                    <span class="badge badge-danger">
                                        <i class="fas fa-times fa-2x"></i>
                                    </span>
                                <?php } ?>
                            </td>
                            <td><?= tanggal(date("D, j F Y H:i:s", $sp->tanggal_cetak)) ?></td>
                        </tr>

                        <?php $i++ ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>