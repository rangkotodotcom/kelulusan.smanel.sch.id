<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Index_model extends CI_model
{
    public function Profil_sekolah()
    {
        return $this->db->get('t_profilsekolah')->row_array();
    }

    public function Blangko_skl()
    {
        return $this->db->get_where('t_blangko', ['id' => '1'])->row_array();
    }

    public function Blangko_kompetensi()
    {
        return $this->db->get_where('t_blangko', ['id' => '2'])->row_array();
    }

    public function Blangko_skbb()
    {
        return $this->db->get_where('t_blangko', ['id' => '3'])->row_array();
    }
}
