<?php defined('BASEPATH') or exit('No direct script access allowed');

class Blangkosurat_model extends CI_Model
{

    private $_table = "t_blangko";

    public $id;
    public $nama_surat;
    public $nomor_surat;
    public $tempat_surat;
    public $tanggal_surat;
    public $ttd;
    public $last_update;

    public function Profil_sekolah()
    {
        return $this->db->get('t_profilsekolah')->row();
    }

    public function getSKL()
    {
        return $this->db->get_where($this->_table, ["id" => "1"])->row();
    }

    public function getKompetensi()
    {
        return $this->db->get_where($this->_table, ["id" => "2"])->row();
    }

    public function getSKBB()
    {
        return $this->db->get_where($this->_table, ["id" => "3"])->row();
    }

    public function update()
    {
        $post = $this->input->post();
        $this->id = $post["id"];
        $this->nama_surat = strtoupper($post["nama_surat"]);
        $this->nomor_surat = $post["nomor_surat"];
        $this->tempat_surat = ucwords($post["tempat_surat"]);
        $this->tanggal_surat = $post["tanggal_surat"];
        if (!empty($_FILES["ttd"]["name"])) {
            $this->ttd = $this->_uploadTTD();
        } else {
            $this->ttd = $post["old_ttd"];
        }
        $this->last_update = NULL;
        $this->db->update($this->_table, $this, ["id" => $post['id']]);

        $data = [
            'kegiatan' => 'Edit Blangko Surat',
            'oleh' => $this->session->userdata('email'),
            'waktu' => NULL
        ];

        $this->db->insert('t_history', $data);
    }

    private function _uploadTTD()
    {
        $config['upload_path']          = './upload/logo/';
        $config['allowed_types']        = 'jpeg|jpg|png';
        $config['file_name']            = $this->nama_surat;
        $config['overwrite']            = true;
        $config['max_size']             = 512; // 0,5MB
        // $config['max_width']            = 314;
        // $config['max_height']           = 220;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('ttd')) {
            return $this->upload->data("file_name");
        }

        return "logo.png";
    }
}
