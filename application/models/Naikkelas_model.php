<?php defined('BASEPATH') or exit('No direct script access allowed');

class Naikkelas_model extends CI_Model
{

    private $_table = "t_naik_kelas";

    public $id;
    public $nisn;
    public $nama;
    public $t_lahir;
    public $tgl_lhr;
    public $nis;
    public $kelas;
    public $ket;

    public function Profil_sekolah()
    {
        return $this->db->get('t_profilsekolah')->row_array();
    }

    public function getAll()
    {
        $system = $this->db->get('t_system')->row_array();
        return $this->db->get_where($this->_table, ["tahun" => $system['tahun_data']])->result();
    }

    public function getById($id)
    {
        $system = $this->db->get('t_system')->row_array();
        return $this->db->get_where($this->_table, ["id" => $id, "tahun" => $system['tahun_data']])->row();
    }

    public function save()
    {
        $post = $this->input->post();
        $this->id = '';
        $this->nama = strtoupper($post["nama"]);
        $this->t_lahir = ucwords($post["t_lahir"]);
        $this->tgl_lhr = $post["tgl_lhr"];
        $this->nis = $post["nis"];
        $this->nisn = $post["nisn"];
        $this->kelas = strtoupper($post["kelas"]);
        $this->ket = strtoupper($post["ket"]);
        $this->tahun = date('Y');
        $this->db->insert($this->_table, $this);

        $data = [
            'kegiatan' => 'Tambah Data (' . $this->nisn = $post["nisn"] . ')',
            'oleh' => $this->session->userdata('email'),
            'waktu' => NULL
        ];

        $this->db->insert('t_history', $data);
    }

    public function update()
    {
        $post = $this->input->post();
        $this->id = $post["id"];
        $this->nama = strtoupper($post["nama"]);
        $this->t_lahir = ucwords($post["t_lahir"]);
        $this->tgl_lhr = $post["tgl_lhr"];
        $this->nis = $post["nis"];
        $this->nisn = $post["nisn"];
        $this->kelas = strtoupper($post["kelas"]);
        $this->ket = $post["ket"];
        $this->tahun = $post["tahun"];
        $this->db->update($this->_table, $this, array("id" => $post['id']));

        $data = [
            'kegiatan' => 'Edit Data (' . $this->nisn = $post["nisn"] . ')',
            'oleh' => $this->session->userdata('email'),
            'waktu' => NULL
        ];

        $this->db->insert('t_history', $data);
    }

    public function delete($id)
    {
        $siswa = $this->db->get_where($this->_table, ["id" => $id])->row_array();

        $data = [
            'kegiatan' => 'Hapus Data (' . $siswa['no_pes'] . ')',
            'oleh' => $this->session->userdata('email'),
            'waktu' => NULL
        ];

        $this->db->insert('t_history', $data);
        return $this->db->delete($this->_table, array("id" => $id));;

        $this->db->insert('t_history', $data);
    }
}
