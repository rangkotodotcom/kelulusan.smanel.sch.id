<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Skbb extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Index_model');
    }

    public function index()
    {

        $data['profilsekolah'] = $this->Index_model->Profil_sekolah();
        $data['title'] = 'SKBB - Pengumuman Kelulusan ' . $data['profilsekolah']['nama_sekolah'];

        $this->load->view('skbb', $data);
    }

    public function cari()
    {
        $nisn = $this->input->post('nisn');

        $this->form_validation->set_rules('nisn', 'NISN', 'required|trim|numeric', [
            'required' => 'NISN wajib di isi!',
            'numeric' => 'NISN harus angka!'
        ]);

        if ($this->form_validation->run() == false) {
            $data['profilsekolah'] = $this->Index_model->Profil_sekolah();
            $data['title'] = 'SKBB - Pengumuman Kelulusan ' . $data['profilsekolah']['nama_sekolah'];

            $this->load->view('skbb', $data);
        } else {

            $data['profilsekolah'] = $this->Index_model->Profil_sekolah();
            $data['system'] = $this->db->get('t_system')->row();

            $cek = $this->db->get_where('t_bio_siswa', ['nisn' => $nisn, 'tahun' => $data['system']->tahun_data])->row_array();

            if ($cek) {
                if ($cek['jurusan'] == "A") {
                    $jur = "MIPA";
                } else {
                    $jur = "IPS";
                }
                $data = [
                    'nama'      => $cek['nama'],
                    't_lahir'   => $cek['t_lahir'],
                    'tgl_lhr'   => $cek['tgl_lhr'],
                    'nisn'      => $cek['nisn'],
                    'nis'       => $cek['nis'],
                    'kelas'     => $cek['kelas'],
                    'jur'       => $jur,
                    'sekolah'   => $data['profilsekolah']['nama_sekolah']
                ];

                $this->session->set_flashdata('hasil', $data);
                redirect(base_url('skbb/'));
            } else {
                $this->session->set_flashdata('alert', 'NISN Tidak Ditemukan');
                redirect(base_url('skbb/'));
            }
        }
    }

    public function cetak($nisn = null)
    {

        if (!isset($nisn)) {
            show_404();
        }

        $data['blangkoskbb'] = $this->Index_model->Blangko_skbb();
        $data['profilsekolah'] = $this->Index_model->Profil_sekolah();
        $data['system'] = $this->db->get('t_system')->row();

        $cek = $this->db->get_where('t_bio_siswa', ['nisn' => $nisn, 'tahun' => $data['system']->tahun_data])->row_array();

        $data['skbb'] = [
            'nama'      => $cek['nama'],
            'tgl_lhr'   => $cek['tgl_lhr'],
            'nisn'      => $cek['nisn'],
            'nis'       => $cek['nis']
        ];

        $history = [
            'nama'          => $cek['nama'],
            'nisn'          => $cek['nisn'],
            'status_cetak'  => '1',
            'tanggal_cetak' => time()
        ];

        $cek_history = $this->db->get_where('t_skbb', ['nisn' => $nisn])->num_rows();

        if ($cek_history < 1) {
            $this->db->insert('t_skbb', $history);
        } else {

            $status_cetak = '1';
            $tanggal_cetak = time();

            $this->db->set('status_cetak', $status_cetak);
            $this->db->set('tanggal_cetak', $tanggal_cetak);
            $this->db->where('nisn', $nisn);
            $this->db->update('t_skbb');
        }

        $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-P', 'margin_left' => 25, 'margin_right' => 25]);

        $mpdf->SetAuthor($data['profilsekolah']['nama_sekolah']);
        $mpdf->SetCreator('ICT ' . $data['profilsekolah']['nama_sekolah']);
        $mpdf->SetSubject($nisn);

        $html = $this->load->view('printskbb', $data, true);
        $mpdf->WriteHTML($html);

        $mpdf->Output('SKBB-' . $nisn . '.pdf', 'D');


        redirect(base_url('skbb/'));
    }
}
