<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Skbbprint extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Admin_model');
        if ($this->session->userdata('email') == '') {
            redirect('auth');
        }

        if ($this->session->userdata('level') != 'admin') {
            redirect(base_url('admin/'));
        }
    }

    public function index()
    {
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['profilsekolah'] = $this->Admin_model->Profil_sekolah();
        $data['title'] = $data['user']['nama'] . ' - Pengumuman Kelulusan ' . $data['profilsekolah']['nama_sekolah'];

        $system = $this->db->get('t_system')->row_array();

        $data['skbbprint'] = $this->db->get('t_skbb')->result();

        $this->load->view('templates/admin_header', $data);
        $this->load->view('skbbprint/view', $data);
        $this->load->view('templates/admin_footer');
    }
}
