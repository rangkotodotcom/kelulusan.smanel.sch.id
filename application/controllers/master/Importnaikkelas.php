<?php
defined('BASEPATH') or exit('No direct script access allowed');

class importnaikkelas extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Admin_model');

        if ($this->session->userdata('email') == '') {
            redirect('auth');
        }

        if ($this->session->userdata('level') != 'admin') {
            redirect(base_url('admin/'));
        }
    }

    public function index()
    {
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $data['profilsekolah'] = $this->Admin_model->Profil_sekolah();
        $data['title'] = $data['user']['nama'] . ' - Pengumuman Kelulusan ' . $data['profilsekolah']['nama_sekolah'];

        if ($this->session->userdata('email') == '') {
            redirect('auth');
        } else {
            $this->load->view('templates/admin_header', $data);
            $this->load->view('master/import/importnaikkelas', $data);
            $this->load->view('templates/admin_footer');
        }
    }

    public function downloadformat()
    {
        force_download('./assets/template/template_import_naik_kelas.xlsx', NULL);
    }

    public function upload()
    {
        $data['system'] = $this->Admin_model->getSystem();
        $drop = $this->input->post('drop') ? $this->input->post('drop') : 0;

        if ($drop == 1) {
            $this->db->delete('t_bio_siswa', ['tahun' => $data['system']['tahun_data']]);
        }


        include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

        $config['upload_path'] = './upload/excel/';
        $config['allowed_types'] = 'xlsx|xls';
        $config['max_size'] = '10000';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {

            //upload gagal
            $this->session->set_flashdata('alert', 'Import Data Siswa Gagal');
            //redirect halaman
            redirect(base_url('master/importnaikkelas/'));
        } else {

            $data_upload = $this->upload->data();

            $excelreader     = new PHPExcel_Reader_Excel2007();
            $loadexcel         = $excelreader->load('./upload/excel/' . $data_upload['file_name']); // Load file yang telah diupload ke folder excel
            $sheet             = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

            $data = array();

            $judul = $sheet[1]['A'];

            if ($judul == 'Blangko Import Naik Kelas') {

                $numrow = 3;
                foreach ($sheet as $row) {

                    $no = $row['A'];
                    $nama = strtoupper($row['B']);
                    $t_lahir = ucwords($row['C']);
                    $tgl_lhr = $row['D'];
                    $nis = $row['E'];
                    $nisn = $row['F'];
                    $kelas = strtoupper($row['G']);
                    $ket = strtoupper($row['H']);

                    if (empty($no) && empty($nama) &&  empty($t_lahir) &&  empty($tgl_lhr) &&  empty($nis) &&  empty($nisn) && empty($kelas) &&  empty($ket)) continue;

                    if ($numrow > 4) {
                        array_push($data, [
                            'nisn'      => $nisn,
                            'nama'      => $nama,
                            't_lahir'   => $t_lahir,
                            'tgl_lhr'   => $tgl_lhr,
                            'nis'       => $nis,
                            'kelas'     => $kelas,
                            'ket'       => $ket,
                            'tahun'     => date('Y'),
                        ]);
                    }
                    $numrow++;
                }

                $history = [
                    'kegiatan' => 'Import Data Siswa',
                    'oleh' => $this->session->userdata('email'),
                    'waktu' => NULL
                ];


                $this->db->insert_batch('t_naik_kelas', $data);
                $this->db->insert('t_history', $history);
                //delete file from server
                unlink('./upload/excel/' . $data_upload['file_name']);

                //upload success
                $this->session->set_flashdata('alert', 'Import Data Siswa Berhasil');
                //redirect halaman
                redirect(base_url('master/naikkelas/'));
            } else {
                //upload gagal
                $this->session->set_flashdata('alert', 'Blangko Import Salah');
                //redirect halaman
                redirect(base_url('master/importnaikkelas/'));
            }
        }
    }
}
