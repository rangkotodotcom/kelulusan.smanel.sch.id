<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Naikkelas extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Naikkelas_model');
        if ($this->session->userdata('email') == '') {
            redirect('auth');
        }

        if ($this->session->userdata('level') != 'admin') {
            redirect(base_url('admin/'));
        }
    }

    public function index()
    {
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['profilsekolah'] = $this->Naikkelas_model->Profil_sekolah();
        $data['title'] = $data['user']['nama'] . ' - Pengumuman Kelulusan ' . $data['profilsekolah']['nama_sekolah'];

        $data['naikkelas'] = $this->Naikkelas_model->getAll();

        $this->load->view('templates/admin_header', $data);
        $this->load->view('master/nkelas/view', $data);
        $this->load->view('templates/admin_footer');
    }

    public function add()
    {

        $this->form_validation->set_rules('nisn', 'NISN', 'required|trim|numeric|max_length[10]', [
            'required' => 'NISN wajib di isi!',
            'numeric' => 'NISN harus angka!',
            'max_length' => 'NISN harus 10 karakter!'
        ]);

        $this->form_validation->set_rules('nama', 'Nama', 'required|trim', [
            'required' => 'Nama wajib di isi!'
        ]);

        $this->form_validation->set_rules('t_lahir', 'Tempat Lahir', 'required|trim', [
            'required' => 'Tempat Lahir wajib di isi!'
        ]);

        $this->form_validation->set_rules('tgl_lhr', 'Tanggal Lahir', 'required|trim', [
            'required' => 'Tanggal Lahir wajib di isi!'
        ]);

        $this->form_validation->set_rules('nis', 'NIS', 'required|trim|numeric', [
            'required' => 'NIS wajib di isi!',
            'numeric' => 'NIS harus angka!'
        ]);

        $this->form_validation->set_rules('kelas', 'Kelas', 'required|trim', [
            'required' => 'Kelas wajib di isi!'
        ]);

        $this->form_validation->set_rules('ket', 'Keterangan', 'required|trim', [
            'required' => 'Keterangan wajib di isi!'
        ]);

        if ($this->form_validation->run() == false) {
            $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
            $data['profilsekolah'] = $this->Naikkelas_model->Profil_sekolah();
            $data['title'] = $data['user']['nama'] . ' - Pengumuman Kelulusan ' . $data['profilsekolah']['nama_sekolah'];

            $this->load->view('templates/admin_header', $data);
            $this->load->view('master/nkelas/tambah', $data);
            $this->load->view('templates/admin_footer');
        } else {
            $system = $this->db->get('t_system')->row_array();
            $cek = $this->db->get_where('t_naik_kelas', ['nisn' => $this->input->post('nisn'), "tahun" => $system['tahun_data']])->num_rows();

            if ($cek < 1) {
                $naikkelas = $this->Naikkelas_model;
                $naikkelas->save();
                $this->session->set_flashdata('alert', 'Data Siswa Berhasil Disimpan');
                redirect(base_url('master/naikkelas/'));
            } else {
                $this->session->set_flashdata('alert', 'Data Siswa Sudah Ada');
                redirect(base_url('master/naikkelas/'));
            }
        }
    }

    public function edit($id = null)
    {

        if (!isset($id)) {
            redirect('master/datadiri');
        }

        $this->form_validation->set_rules('nisn', 'NISN', 'required|trim|numeric|max_length[10]', [
            'required' => 'NISN wajib di isi!',
            'numeric' => 'NISN harus angka!',
            'max_length' => 'NISN harus 10 karakter!'
        ]);

        $this->form_validation->set_rules('nama', 'Nama', 'required|trim', [
            'required' => 'Nama wajib di isi!'
        ]);

        $this->form_validation->set_rules('t_lahir', 'Tempat Lahir', 'required|trim', [
            'required' => 'Tempat Lahir wajib di isi!'
        ]);

        $this->form_validation->set_rules('tgl_lhr', 'Tanggal Lahir', 'required|trim', [
            'required' => 'Tanggal Lahir wajib di isi!'
        ]);

        $this->form_validation->set_rules('nis', 'NIS', 'required|trim|numeric', [
            'required' => 'NIS wajib di isi!',
            'numeric' => 'NIS harus angka!'
        ]);

        $this->form_validation->set_rules('kelas', 'Kelas', 'required|trim', [
            'required' => 'Kelas wajib di isi!'
        ]);

        $this->form_validation->set_rules('ket', 'Keterangan', 'required|trim', [
            'required' => 'Keterangan wajib di isi!'
        ]);

        if ($this->form_validation->run() == false) {
            $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
            $data['profilsekolah'] = $this->Naikkelas_model->Profil_sekolah();
            $data['title'] = $data['user']['nama'] . ' - Pengumuman Kelulusan ' . $data['profilsekolah']['nama_sekolah'];
            $naikkelas = $this->Naikkelas_model;
            $data["naikkelas"] = $naikkelas->getById($id);
            if (!$data["naikkelas"]) show_404();
            $this->load->view('templates/admin_header', $data);
            $this->load->view('master/nkelas/edit', $data);
            $this->load->view('templates/admin_footer');
        } else {
            $naikkelas = $this->Naikkelas_model;
            $naikkelas->update();
            $this->session->set_flashdata('alert', 'Data Siswa Berhasil Diedit');
            redirect(base_url('master/naikkelas/'));
        }
    }

    public function delete($id = null)
    {
        if (!isset($id)) show_404();

        if ($this->Naikkelas_model->delete($id)) {
            redirect(site_url('master/naikkelas/'));
        }
    }
}
