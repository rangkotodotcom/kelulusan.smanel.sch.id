<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Nkelas extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Nkelas_model');
    }

    public function index()
    {
        $data['profilsekolah'] = $this->Nkelas_model->Profil_sekolah();

        $data['title'] = 'Pengumuman Kenaikan ' .  $data['profilsekolah']['nama_sekolah'];
        $data['system'] = $this->db->get('t_system')->row();

        $this->load->view('templates/index_header', $data);
        $this->load->view('public/nkelas', $data);
        $this->load->view('templates/index_footer', $data);
    }

    public function hasil()
    {
        $this->form_validation->set_rules('nisn', 'NISN', 'required|trim', [
            'required' => 'NISN wajib di isi!'
        ]);

        $this->form_validation->set_rules('tgl_lhr', 'Tanggal Lahir', 'required|trim', [
            'required' => 'Tanggal Lahir wajib di isi!'
        ]);

        if ($this->form_validation->run() == false) {
            $data['profilsekolah'] = $this->Nkelas_model->Profil_sekolah();

            $data['title'] = 'Pengumuman Kenaikan ' .  $data['profilsekolah']['nama_sekolah'];
            $data['system'] = $this->db->get('t_system')->row();

            $this->load->view('templates/index_header', $data);
            $this->load->view('public/nkelas', $data);
            $this->load->view('templates/index_footer');
        } else {
            $this->_hasil();
        }
    }

    private function _hasil()
    {
        $nisn = $this->input->post('nisn');
        $tgl_lhr = $this->input->post('tgl_lhr');
        $data['system'] = $this->db->get('t_system')->row();

        $profilsekolah = $this->Nkelas_model->Profil_sekolah();

        $cek_nisn = $this->db->get_where('t_naik_kelas', ['nisn' => $nisn, 'tahun' => $data['system']->tahun_data])->row_array();

        if ($cek_nisn) {
            if ($tgl_lhr == $cek_nisn['tgl_lhr']) {

                if ($cek_nisn['ket'] == 'N') {
                    $ket = "Naik Kelas";
                } else {
                    $ket = "Tidak Naik Kelas";
                }

                $this->load->library('ciqrcode');

                $config['cacheable']    = true; //boolean, the default is true
                $config['cachedir']     = './upload/qrcode/cache/'; //string, the default is application/cache/
                $config['errorlog']     = './upload/qrcode/log/'; //string, the default is application/logs/
                $config['imagedir']     = './upload/qrcode/'; //direktori penyimpanan qr code
                $config['quality']      = true; //boolean, the default is true
                $config['size']         = '1024'; //interger, the default is 1024
                $config['black']        = array(224, 255, 255); // array, default is array(255,255,255)
                $config['white']        = array(70, 130, 180); // array, default is array(0,0,0)
                $this->ciqrcode->initialize($config);

                $image_name = $nisn . '.png'; //buat name dari qr code sesuai dengan no_pes

                $data_code = 'Siswa yang bernama ' . $cek_nisn['nama'] . ' dengan NISN ' . $nisn . ' Dinyatakan ' . $ket . ' dari ' . $profilsekolah['nama_sekolah'];

                $params['data'] = $data_code; //data yang akan di jadikan QR CODE
                $params['level'] = 'H'; //H=High
                $params['size'] = 10;
                $params['savename'] = FCPATH . $config['imagedir'] . $image_name; //simpan image QR CODE ke folder upload/qrcode/
                $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE

                $data =  [
                    'nisn'      => $cek_nisn['nisn'],
                    'nama'      => $cek_nisn['nama'],
                    't_lahir'   => $cek_nisn['t_lahir'],
                    'tgl_lhr'   => $cek_nisn['tgl_lhr'],
                    'nis'       => $cek_nisn['nis'],
                    'kelas'     => $cek_nisn['kelas'],
                    'ket'       => $cek_nisn['ket'],
                    'qr_code'   => base_url('upload/qrcode/') . $image_name
                ];

                $this->_printsk($data);
            } else {
                $this->session->set_flashdata('alert', 'Tanggal Lahir Tidak Sesuai');
                redirect(base_url());
            }
        } else {
            $this->session->set_flashdata('alert', 'No Peserta Tidak Ditemukan');
            redirect(base_url());
        }
    }

    private function _printsk($datask)
    {

        $data['profilsekolah'] = $this->Nkelas_model->Profil_sekolah();

        $data['title'] = 'Pengumuman Kenaikan ' .  $data['profilsekolah']['nama_sekolah'];

        $data['system'] = $this->db->get('t_system')->row();

        $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-P', 'margin_left' => 30, 'margin_right' => 30, 'margin_top' => 25]);

        $mpdf->SetWatermarkImage(base_url('assets/img/pdd.png'));
        $mpdf->showWatermarkImage = true;
        $mpdf->watermarkImageAlpha = 0.2;

        $mpdf->SetAuthor($data['profilsekolah']['nama_sekolah']);
        $mpdf->SetCreator('ICT ' . $data['profilsekolah']['nama_sekolah']);
        $mpdf->SetSubject($data['nisn']);

        $data['data'] = $datask;

        $html = $this->load->view('public/printsk_dy', $data, true);
        $mpdf->WriteHTML($html);
        $mpdf->SetHTMLFooter('
<table width="100%" style="border-top:1px solid; font-size:10px;">
    <tr>
        <td width="37%"><i>' . $datask["nama"] . '</i></td>
        <td width="27%" align="center">' .  $data['profilsekolah']['nama_sekolah'] . '</td>
        <td width="37%" align="right"><i>' . $datask["nisn"] . '</i></td>
        
    </tr>
</table>');

        $mpdf->Output('SK-' . $datask['nisn'] . '.pdf', 'D');

        redirect(base_url());
    }
}
