<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Admin_model');
    }

    public function index()
    {
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $data['profilsekolah'] = $this->Admin_model->Profil_sekolah();
        $data['title'] = $data['user']['nama'] . ' - Pengumuman Kelulusan ' . $data['profilsekolah']['nama_sekolah'];

        $data['system'] = $this->Admin_model->getSystem();
        $data['jumlahsiswa'] = $this->Admin_model->getJumlahsiswa();
        $data['contactperson'] = $this->Admin_model->getContactperson();
        $data['jumlahsiswapustaka'] = $this->Admin_model->getJumlahsiswapustaka();
        $data['jumlahsiswatatausaha'] = $this->Admin_model->getJumlahsiswatatausaha();
        $data['informasiadmin'] = $this->Admin_model->getInformasiadmin();
        $data['informasipustaka'] = $this->Admin_model->getInformasipustaka();
        $data['informasitatausaha'] = $this->Admin_model->getInformasitatausaha();

        if ($this->session->userdata('email') == '') {
            redirect('auth');
        } else {
            $this->load->view('templates/admin_header', $data);
            $this->load->view('admin/index', $data);
            $this->load->view('templates/admin_footer');
        }
    }
}
